module.exports = {
    preset: '@vue/cli-plugin-unit-jest',
    transform: {
        '^.+\\vue$': 'vue-jest',
        "^.+\\.js$": "babel-jest",
    },
    moduleFileExtensions: ["js", "json", "vue"],
    moduleNameMapper: {
        "\\.(css|less)$": "/Users/vicente/Documents/Creditu-Apply-Test/creditu-test-app-csr/tests/unit/utils/styleMocks.js"
    }
    
}
