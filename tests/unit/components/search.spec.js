
import {shallowMount} from '@vue/test-utils';
import Search from '@/modules/components/Search.vue';


describe('Search component', () =>{
    
    let wrapper
    let clgSpy
    
    test('It should match with snapshot', () =>{

         wrapper = shallowMount(Search)

        expect(wrapper.html()).toMatchSnapshot()

    })

    test('escribir el simbolo de "?" debe de disparar el getAnswer ', async() => {
        
        const getAnswerSpy = jest.spyOn( wrapper.vm, 'ais-hits' )

        const input = wrapper.find('ais-search-box')
        await input.setValue('fgzw')

        expect( clgSpy ).toHaveBeenCalledTimes(10)
        expect( getAnswerSpy ).toHaveBeenCalled()
    })

})




