# Creditu-test-app-csr

Creditu-test-app-ssr it is an application used to find players through text matches in their nicknames and status

## Installation

Project setup

```bash
npm install
```
Compiles and hot-reloads for development

```bash
npm run serve
```
Compiles and minifies for production

```bash
npm run build
```
To run your tests

```bash
npm run test:unit
```

```bash
npm run test:e2e
```

## Usage

```javascript
import foobar

//Algolia credentials
const searchClient = algoliasearch(
   AppId,
   ApiKey
);

//Choose Index 
createServerRootMixin({
        indexName: 'creditu_players',
        searchClient,
        routing: {}
})
```

```xml
<!--Render data on DOM-->
<ais-hits>
      <template slot="item" slot-scope="{ item }">
        <p><ais-highlight attribute="nickanme" :hit="item" /></p>
        <p><ais-highlight attribute="status" :hit="item" /></p>
      </template>
</ais-hits>
```

## Technology
Algolia's technology is used to find matches through its smart search algorithms

Server-side rendering (SSR) technology is a technique used to represent the results of a client-side framework in a server-side language.


## License
[MIT](https://choosealicense.com/licenses/mit/)